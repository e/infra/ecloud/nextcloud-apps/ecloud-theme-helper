<?php

namespace OCA\EcloudThemeHelper\Migration;

use OCA\EcloudThemeHelper\Mail\EMailTemplate;
use OCP\IConfig;
use OCP\Migration\IOutput;
use OCP\Migration\IRepairStep;

class SetEmailTemplate implements IRepairStep {
	/** @var IConfig */
	protected $config;

	public function __construct(IConfig $config) {
		$this->config = $config;
	}

	public function getName() {
		return 'Set the custom email template';
	}

	public function run(IOutput $output) {
		$this->config->setSystemValue('mail_template_class', EMailTemplate::class);
	}
}
