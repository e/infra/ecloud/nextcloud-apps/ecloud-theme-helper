document.addEventListener('DOMContentLoaded', function() {
	// Removing all elements with class 'settings-hint' except the first one
	const hints = document.querySelectorAll('#two-factor-auth .settings-hint')
	hints.forEach(hint => {
		hint.remove()
	})

	// Updating the href attribute of the element with class 'icon-info'
	const infoIcon = document.querySelector('#two-factor-auth > a.icon-info')
	infoIcon.setAttribute('href', 'https://doc.e.foundation/support-topics/two-factor-authentication#using-an-application-password-for-the-eos-account-manager')

	// Creating a new element and inserting it after the first element with class 'settings-hint'
	const newHint = document.createElement('p')
	newHint.className = 'margin-top-10 settings-hint hidden'
	newHint.innerHTML = t('settings', '<b>Note:</b> You\'ll need to create a new <a class="text-color-active" href="https://doc.e.foundation/support-topics/two-factor-authentication#using-an-application-password-for-the-eos-account-manager">application password</a> to set your account within /e/OS. Additionally, you need to <a class="text-color-active" href="https://doc.e.foundation/support-topics/two-factor-authentication#manually-setup-mail-application">provide your main account password to /e/OS Mail</a>.')
	infoIcon.insertAdjacentElement('afterend', newHint)

	// Removing the 'hidden' class from all elements with class 'settings-hint'
	const allHints = document.querySelectorAll('.settings-hint')

	allHints.forEach(hint => {
		hint.classList.remove('hidden')
	})

})
