<?php

namespace OCA\EcloudThemeHelper\Listeners;

use OCP\EventDispatcher\Event;
use OCP\AppFramework\Http\Events\BeforeTemplateRenderedEvent;
use \OCP\EventDispatcher\IEventListener;
use OCP\Util;
use OCP\IUserSession;
use OCP\IRequest;
use OCP\App\IAppManager;

class BeforeTemplateRenderedListener implements IEventListener {
	private Util $util;
	private IUserSession $userSession;
	private IRequest $request;
	private string $appName;
	private IAppManager $appManager;

	public function __construct($appName, Util $util, IUserSession $userSession, IRequest $request, IAppManager $appManager) {
		$this->appName = $appName;
		$this->util = $util;
		$this->userSession = $userSession;
		$this->request = $request;
		$this->appManager = $appManager;
	}
	public function handle(Event $event): void {
		if (!($event instanceof BeforeTemplateRenderedEvent)) {
			return;
		}
		$pathInfo = $this->request->getPathInfo();
		$this->util->addStyle($this->appName, 'icons');
		$this->util->addStyle($this->appName, 'icons-vars');

		if ($this->userSession->isLoggedIn()) {
			$this->util->addScript($this->appName, $this->appName . '-unified-search-encryption-message');
			$this->util->addStyle($this->appName, 'unified-search-encryption-message');
		}
		if (strpos($pathInfo, '/apps/files/') !== false) {
			$this->util->addStyle($this->appName, 'files');
			$this->util->addScript($this->appName, $this->appName . '-files-icon-alignment');
		}
		if (strpos($pathInfo, '/apps/activity/') !== false) {
			$this->util->addStyle($this->appName, 'activity');
		}
		if (strpos($pathInfo, '/apps/cookbook/') !== false) {
			$this->util->addStyle($this->appName, 'cookbook');
		}
		if (strpos($pathInfo, '/apps/contacts/') !== false) {
			$this->util->addStyle($this->appName, 'contacts');
		}
		if (strpos($pathInfo, '/apps/calendar/') !== false) {
			$this->util->addStyle($this->appName, 'calendar');
		}
		if (strpos($pathInfo, '/apps/news') !== false) {
			$this->util->addStyle($this->appName, 'news');
		}
		if (strpos($pathInfo, '/apps/notes/') !== false) {
			$this->util->addStyle($this->appName, 'notes');
		}
		if (strpos($pathInfo, '/apps/deck/') !== false) {
			$this->util->addStyle($this->appName, 'deck');
		}
		if (strpos($pathInfo, '/apps/bookmarks/') !== false) {
			$this->util->addStyle($this->appName, 'bookmarks');
		}
		if (strpos($pathInfo, '/apps/tasks/') !== false) {
			$this->util->addStyle($this->appName, 'tasks');
		}
		if (strpos($pathInfo, '/apps/photos/') !== false) {
			$this->util->addStyle($this->appName, 'photos');
		}
		if ($pathInfo === '/settings/user/theming') {
			$this->util->addStyle($this->appName, 'settings-theming');
		}
		if (strpos($pathInfo, '/login/challenge/totp') !== false) {
			$this->util->addStyle($this->appName, 'twofactor-totp');
		}
		if (strpos($pathInfo, '/settings/user/workflow') !== false) {
			$this->util->addStyle($this->appName, 'workflow');
		}

		$response = $event->getResponse();
		if ($pathInfo === '/settings/user' && $response->getTemplateName() === 'settings/frame') {
			$this->util->addScript($this->appName, $this->appName . '-personal-info-settings');
			$this->util->addStyle($this->appName, 'personal-info-settings');
		}
		if (strpos($pathInfo, '/settings/') !== false ||
			strpos($pathInfo, '/apps/activity') !== false) {
			$this->util->addStyle($this->appName, 'navigation-icons');
		}

		if ($pathInfo === '/settings/user/security') {
			$this->util->addScript($this->appName, $this->appName . '-settings-user-security');
			$this->util->addStyle($this->appName, 'settings-security');
		}
		if ($pathInfo === '/settings/user/notifications') {
			$this->util->addStyle($this->appName, 'settings-notifications');
		}
		if ($pathInfo === '/settings/user/drop_account' && $response->getTemplateName() === 'settings/frame') {
			$this->util->addStyle($this->appName, 'settings-drop-account');
		}
		if (strpos($pathInfo, '/apps/snappymail/') !== false) {
			$this->util->addStyle($this->appName, 'snappymail');
		}
		if (strpos($pathInfo, '/settings/user/availability') !== false) {
			$this->util->addStyle($this->appName, 'settings-groupware');
		}
		if (strpos($pathInfo, '/apps/memories/') !== false) {
			$this->util->addStyle($this->appName, 'memories');
		}
		if (strpos($pathInfo, '/apps/forms/') !== false) {
			$this->util->addStyle($this->appName, 'forms');
		}
		if ($pathInfo === '/settings/user/privacy') {
			$this->util->addStyle($this->appName, 'settings-privacy');
		}
		if ($pathInfo === '/settings/help') {
			$this->util->addStyle($this->appName, 'settings-help');
		}
	}
}
