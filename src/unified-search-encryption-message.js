document.addEventListener('DOMContentLoaded', function() {
	const headerElement = document.querySelector('#header')
	if (!headerElement) {
		return
	}
	const searchHelpText = t('core', 'Due to encryption the search is only done on files\' name.')
	const attr = '#unified-search .empty-content:after, #unified-search .unified-search-modal__results:after'
	const style = `{ content: "${searchHelpText.replace(/'/g, '\\\'')}" }` // Correctly escape single quotes
	const newStyle = document.createElement('style')
	newStyle.innerHTML = attr + style
	headerElement.appendChild(newStyle)
})
