// SPDX-FileCopyrightText: {{ app.author_name }} <{{ app.author_mail }}>
// SPDX-License-Identifier: {{ app.license }}
const webpackConfig = require('@nextcloud/webpack-vue-config')
const path = require('path')

module.exports = {
	...webpackConfig,
	entry: {
		'personal-info-settings': path.join(__dirname, 'src/personal-info-settings.js'),
		'settings-user-security': path.join(__dirname, 'src/settings-user-security.js'),
		'files-icon-alignment': path.join(__dirname, 'src/files-icon-alignment.js'),
		'unified-search-encryption-message': path.join(__dirname, 'src/unified-search-encryption-message.js')
	},
}
