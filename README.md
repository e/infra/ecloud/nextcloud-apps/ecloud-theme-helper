## /e/ Cloud Theme Helper App
- This app is a helper app to be used along with the /e/ Cloud Theme 
- It adds icon classes and makes the needed icon replacements in some Nextcloud pages
- Enabling it without adding and enabling the /e/ Cloud Theme is not recommended


## Developer Documentation

### To add new icons

- Add the icon in the `img` folder
  - For example, the `notifications.svg` file will be added as `img/notifications/notifications.svg`
- Add entry to `src/icons.js`
  - For a colored icon, add to `iconsColor` object
  - For a white/black icon, add to `icons` object
- Icon will now be generated and added to `icons.css` in the pre-build step
