/* eslint-disable quote-props */
import path from 'path'
import fs from 'fs'
import sass from 'sass'

const colors = {
	dark: '000',
	white: 'fff',
	yellow: 'FC0',
	red: 'e9322d',
	orange: 'eca700',
	green: '46ba61',
	grey: '969696',
	lightgrey: '808080',
}

const variables = {}

/*
	Example entry in icons object will be like:
	{
		...
		'add': path.join(__dirname, '../img', 'actions', 'add.svg'),
		...
	}
*/
const icons = {
	'search': path.join(__dirname, '../img', 'actions', 'search.svg'),
	'notifications': path.join(__dirname, '../img', 'notifications', 'notifications.svg'),
}

/*
	Example entry in iconsColor object will be like:
	{
		...
		'search': {
			path: path.join(__dirname, '../img', 'actions', 'search.svg'),
			color: 'lightgrey'
		}
		...
	}
*/
const iconsColor = {
	'search': {
		path: path.join(__dirname, '../img', 'actions', 'search.svg'),
		color: 'lightgrey',
	},
	'notifications': {
		path: path.join(__dirname, '../img', 'notifications', 'notifications.svg'),
		color: 'lightgrey',
	},
}

// use this to define aliases to existing icons
// key is the css selector, value is the variable
const iconsAliases = {
}

const colorSvg = function(svg = '', color = '000') {
	if (!color.match(/^[0-9a-f]{3,6}$/i)) {
		// Prevent not-sane colors from being written into the SVG
		console.warn(color, 'does not match the required format')
		color = '000'
	}

	// add fill (fill is not present on black elements)
	const fillRe = /<((circle|rect|path)((?!fill)[a-z0-9 =".\-#():;,])+)\/>/gmi
	svg = svg.replace(fillRe, '<$1 fill="#' + color + '"/>')

	// replace any fill or stroke colors
	svg = svg.replace(/stroke="#([a-z0-9]{3,6})"/gmi, 'stroke="#' + color + '"')
	svg = svg.replace(/fill="#([a-z0-9]{3,6})"/gmi, 'fill="#' + color + '"')

	return svg
}

const generateVariablesAliases = function(invert = false) {
	let css = ''
	Object.keys(variables).forEach(variable => {
		if (variable.indexOf('original-') !== -1) {
			let finalVariable = variable.replace('original-', '')
			if (invert) {
				finalVariable = finalVariable.replace('white', 'tempwhite')
					.replace('dark', 'white')
					.replace('tempwhite', 'dark')
			}
			css += `${finalVariable}: var(${variable});`
		}
	})
	return css
}

const formatIcon = function(icon, invert = false) {
	const color1 = invert ? 'white' : 'dark'
	const color2 = invert ? 'dark' : 'white'
	return `
	.icon-${icon},
	.icon-${icon}-dark {
		background-image: var(--icon-${icon}-${color1});
	}
	.icon-${icon}-white,
	.icon-${icon}.icon-white {
		background-image: var(--icon-${icon}-${color2});
	}`
}
const formatIconColor = function(icon) {
	const { color } = iconsColor[icon]
	return `
	.icon-${icon} {
		background-image: var(--icon-${icon}-${color});
	}`
}
const formatAlias = function(alias, invert = false) {
	let icon = iconsAliases[alias]
	if (invert) {
		icon = icon.replace('white', 'tempwhite')
			.replace('dark', 'white')
			.replace('tempwhite', 'dark')
	}
	return `
	.${alias} {
		background-image: var(--${icon})
	}`
}

let css = ''
Object.keys(icons).forEach(icon => {
	const path = icons[icon]

	const svg = fs.readFileSync(path, 'utf8')
	const darkSvg = colorSvg(svg, '000000')
	const whiteSvg = colorSvg(svg, 'ffffff')

	variables[`--original-icon-${icon}-dark`] = Buffer.from(darkSvg, 'utf-8').toString('base64')
	variables[`--original-icon-${icon}-white`] = Buffer.from(whiteSvg, 'utf-8').toString('base64')
})

Object.keys(iconsColor).forEach(icon => {
	const { path, color } = iconsColor[icon]

	const svg = fs.readFileSync(path, 'utf8')
	const coloredSvg = colorSvg(svg, colors[color])
	variables[`--icon-${icon}-${color}`] = Buffer.from(coloredSvg, 'utf-8').toString('base64')
})

// ICONS VARIABLES LIST
css += ':root {'
Object.keys(variables).forEach(variable => {
	const data = variables[variable]
	css += `${variable}: url(data:image/svg+xml;base64,${data});`
})
css += '}'

// DEFAULT THEME
css += 'body {'
css += generateVariablesAliases()
Object.keys(icons).forEach(icon => {
	css += formatIcon(icon)
})
Object.keys(iconsColor).forEach(icon => {
	css += formatIconColor(icon)
})
Object.keys(iconsAliases).forEach(alias => {
	css += formatAlias(alias)
})
css += '}'

// DARK THEME MEDIA QUERY
css += '@media (prefers-color-scheme: dark) { body {'
css += generateVariablesAliases(true)
css += '}}'

// DARK THEME
css += '[data-themes*=light] {'
css += generateVariablesAliases()
css += '}'

// DARK THEME
css += '[data-themes*=dark] {'
css += generateVariablesAliases(true)
css += '}'

// WRITE CSS
fs.writeFileSync(path.join(__dirname, '../css', 'icons.css'), sass.compileString(css).css)
