<?php

namespace OCA\ECloudThemeHelper\Mail;

use OC\Mail\EMailTemplate as ParentTemplate;

class EMailTemplate extends ParentTemplate {
	protected string $header = <<<EOF
<table align="center" class="container main-heading float-center" style="Margin:0 auto;background:0 0!important;border-collapse:collapse;border-spacing:0;float:none;margin-top:20px;padding:0;text-align:center;vertical-align:top;width:auto;">
<tbody>
<tr style="border: 1px solid #EAEDF2;box-sizing: border-box;height:70px;">
<td>
<table align="center" class="container main-heading float-center" style="Margin:0 auto;background:0 0!important;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%%">
<tbody>
<tr style="padding:0;text-align:left;vertical-align:top">
    <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
        <img class="logo float-center" src="%s" alt="%s" align="center" style="-ms-interpolation-mode:bicubic;clear:both;display:block;float:right;margin:0 auto;outline:0;text-align:center;text-decoration:none;max-height:105px;max-width:105px;width:auto;padding-top:4px;margin-left:10px;padding-right:30px">
EOF;

	protected string $heading = <<<EOF
<h1 class="text-center" style="Margin:0;Margin-bottom:10px;color:inherit;font-family:arial;font-size:24px;font-weight:400;color:#000000;line-height:1.3;margin:0;padding:0;text-align:left;word-wrap:normal;padding-right:135px;padding-left:40px;">%s</h1>
    </td>
</tr>
</tbody>
</table>
</td>
</tr>
EOF;

	protected string $bodyBegin = <<<EOF
<tr style="border: 1px solid #EAEDF2;box-sizing: border-box;">
<td>
<table class="spacer float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%%">
	<tbody>
	<tr style="padding:0;text-align:left;vertical-align:top;height: 84px;">
		<td height="36px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-size:40px;font-weight:400;hyphens:auto;line-height:36px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:middle;word-wrap:break-word">&#xA0;</td>
	</tr>
	</tbody>
</table>
<table align="center" class="wrapper content float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%">
<tr style="padding:0;text-align:left;vertical-align:top">
    <td class="wrapper-inner" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
        <table align="center" class="container" style="Margin:0 auto;background:#fff;border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0;text-align:inherit;vertical-align:top;width:auto;">
            <tbody>
            <tr style="padding:0;text-align:left;vertical-align:top;">
                <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
EOF;

	protected string $bannerHeading = <<<EOF
<tr style="border: 1px solid #EAEDF2;box-sizing: border-box;">
<td>
<table class="spacer float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%%">
	<tbody>
	<tr style="padding:0;text-align:left;vertical-align:top">
		<td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-size:40px;font-weight:400;hyphens:auto;line-height:23px;margin:0;mso-line-height-rule:exactly;padding:30px 30px 30px 40px;text-align:left;vertical-align:top;word-wrap:break-word;background:%s;font-size:18px;font-weight:500;color:%s">%s</td>
	</tr>
	</tbody>
</table>
</td></tr>
EOF;

	protected string $buttonGrouptentative = <<<EOF
<table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%%">
	<tbody>
	<tr style="padding:0;text-align:left;vertical-align:top">
		<td height="50px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:50px;font-weight:400;hyphens:auto;line-height:50px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td>
	</tr>
	</tbody>
</table>
<table align="center" class="row btn-group" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%%">
	<tbody>
	<tr style="padding:0;text-align:left;vertical-align:top">
		<th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:30px;padding-left:30px;padding-right:30px;text-align:left;width:auto;">
			<table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%%">
				<tr style="padding:0;text-align:left;vertical-align:top">
					<th style="Margin:0;color:#0a0a0a;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
						<center data-parsed="" style="min-width:490px;width:100%%">
							<table class="button btn default primary float-center" style="Margin:0 0 30px 0;border-collapse:collapse;border-spacing:0;display:inline-block;float:none;margin:0 0 30px 0;margin-right:15px;max-height:60px;max-width:300px;padding:0;text-align:center;vertical-align:top;width:auto;background:%1\$s;background-color:%1\$s;color:#fefefe;">
								<tr style="padding:0;text-align:left;vertical-align:top">
									<td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
										<table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%%">
											<tr style="padding:0;text-align:left;vertical-align:top">
												<td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border:0 solid %2\$s;border-collapse:collapse!important;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
													<a href="%3\$s" style="Margin:0;border:0 solid %4\$s;border-radius:2px;color:%5\$s;display:inline-block;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:16px;font-weight:regular;line-height:1.3;margin:0;padding:10px 10px 10px 10px;text-align:left;outline:1px solid %6\$s;text-decoration:none">%7\$s</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<table class="button btn default secondary float-center" style="Margin:0 0 30px 0;border-collapse:collapse;border-spacing:0;display:inline-block;float:none;margin:0 0 30px 0;margin-right:15px;max-height:60px;max-width:300px;padding:0;text-align:center;vertical-align:top;">
								<tr style="padding:0;text-align:left;vertical-align:top">
									<td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
										<table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%%">
											<tr style="padding:0;text-align:left;vertical-align:top">
												<td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background:#ffffff;border:0 solid #666666;border-collapse:collapse!important;color:#fefefe;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
													<a href="%10\$s" style="Margin:0;background-color:#ffffff;border:0 solid #666666;border-radius:2px;color:#666666!important;display:inline-block;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:16px;font-weight:regular;line-height:1.3;margin:0;outline:1px solid #666666;padding:10px 10px 10px 10px;text-align:left;text-decoration:none">%11\$s</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<table class="button btn default secondary float-center" style="Margin:0 0 30px 0;border-collapse:collapse;border-spacing:0;display:inline-block;float:none;margin:0 0 30px 0;margin-right:15px;max-height:60px;max-width:300px;padding:0;text-align:center;vertical-align:top;">
								<tr style="padding:0;text-align:left;vertical-align:top">
									<td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
										<table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%%">
											<tr style="padding:0;text-align:left;vertical-align:top">
												<td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background:#ffffff;border:0 solid #666666;border-collapse:collapse!important;color:#fefefe;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
													<a href="%8\$s" style="Margin:0;background-color:#ffffff;border:0 solid #666666;border-radius:2px;color:#666666!important;display:inline-block;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:16px;font-weight:regular;line-height:1.3;margin:0;outline:1px solid #666666;padding:10px 10px 10px 10px;text-align:left;text-decoration:none">%9\$s</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</center>
					</th>
					<th class="expander" style="Margin:0;color:#0a0a0a;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
				</tr>
			</table>
		</th>
	</tr>
	</tbody>
</table>
EOF;
	/**
	 * Adds a header to the email
	 */
	public function addHeader(): void {
		if ($this->headerAdded) {
			return;
		}
		$this->headerAdded = true;

		$logoUrl = $this->urlGenerator->getAbsoluteURL(
			$this->themingDefaults->getLogo(false)
		);
		$logoMailUrl = str_replace("logo.png", "logomail.png", $logoUrl);
		$this->htmlBody .= vsprintf($this->header, [
			$logoMailUrl,
			$this->themingDefaults->getName(),
		]);
	}

	/**
	 * Adds a header banner
	 */
	public function addHeadingBanner(
		string $bgcolor,
		string $fontcolor,
		string $text,
		$plainText = ""
	): void {
		$this->htmlBody .= vsprintf($this->bannerHeading, [
			$bgcolor,
			$fontcolor,
			$text,
		]);
		if ($plainText !== false) {
			$this->plainBody .= $plainText . PHP_EOL . PHP_EOL;
		}
		//$this->bodyOpened = true;
		$this->ensureBodyListOpened();
	}

	/**
	 * Adds a list item to the body of the email
	 *
	 * @param string $text Note: When $plainText falls back to this, HTML is automatically escaped in the HTML email
	 * @param string $metaInfo Note: When $plainMetaInfo falls back to this, HTML is automatically escaped in the HTML email
	 * @param string $icon Absolute path, must be 16*16 pixels
	 * @param string|bool $plainText Text that is used in the plain text email
	 *   if empty or true the $text is used, if false none will be used
	 * @param string|bool $plainMetaInfo Meta info that is used in the plain text email
	 *   if empty or true the $metaInfo is used, if false none will be used
	 * @param integer plainIndent If > 0, Indent plainText by this amount.
	 * @since 12.0.0
	 */
	public function addBodyListItem(
		string $text,
		string $metaInfo = "",
		string $icon = "",
		$plainText = "",
		$plainMetaInfo = "",
		$plainIndent = 0
	): void {
		$this->ensureBodyListOpened();

		if ($plainText === "" || $plainText === true) {
			$plainText = $text;
			$text = htmlspecialchars($text);
			$text = str_replace("\n", "<br/>", $text); // convert newlines to HTML breaks
		}
		if ($plainMetaInfo === "" || $plainMetaInfo === true) {
			$plainMetaInfo = $metaInfo;
			$metaInfo = htmlspecialchars($metaInfo);
		}

		$htmlText = $text;
		if ($metaInfo) {
			$htmlText =
				'<span style="color:#777;font-weight:700;">' .
				$metaInfo .
				" </span>  " .
				$htmlText;
		}
		if ($icon !== "") {
			$icon =
				'<span style="display:block;width:16px;padding-bottom:25px;"><img style="padding-top: 3px;" src="' .
				htmlspecialchars($icon) .
				'" alt="&bull;"></span>';
		} else {
			$icon = "&bull;";
		}

		$this->htmlBody .= vsprintf($this->listItem, [$icon, $htmlText]);
		if ($plainText !== false) {
			if ($plainIndent === 0) {
				/*
		 * If plainIndent is not set by caller, this is the old NC17 layout code.
		 */
				$this->plainBody .= "  * " . $plainText;
				if ($plainMetaInfo !== false) {
					$this->plainBody .= " (" . $plainMetaInfo . ")";
				}
				$this->plainBody .= PHP_EOL;
			} else {
				/*
		 * Caller can set plainIndent > 0 to format plainText in tabular fashion.
		 * with plainMetaInfo in column 1, and plainText in column 2.
		 * The plainMetaInfo label is right justified in a field of width
		 * "plainIndent". Multilines after the first are indented plainIndent+1
		 * (to account for space after label).  Fixes: #12391
		 */
				/** @var string $label */
				$label = $plainMetaInfo !== false ? $plainMetaInfo : "";
				$this->plainBody .= sprintf(
					"%{$plainIndent}s %s\n",
					$label,
					str_replace(
						"\n",
						"\n" . str_repeat(" ", $plainIndent + 1),
						$plainText
					)
				);
			}
		}
	}
	/**
	 * Adds a list item to the body of the email
	 *
	 * @param string $text Note: When $plainText falls back to this, HTML is automatically escaped in the HTML email
	 * @param string $metaInfo Note: When $plainMetaInfo falls back to this, HTML is automatically escaped in the HTML email
	 * @param string $icon Absolute path, must be 16*16 pixels
	 * @param string|bool $plainText Text that is used in the plain text email
	 *   if empty or true the $text is used, if false none will be used
	 * @param string|bool $plainMetaInfo Meta info that is used in the plain text email
	 *   if empty or true the $metaInfo is used, if false none will be used
	 * @param integer plainIndent If > 0, Indent plainText by this amount.
	 * @since 12.0.0
	 */
	public function addBodyListItemModified(
		string $text,
		string $metaInfo = "",
		string $icon = "",
		$plainText = "",
		$plainMetaInfo = "",
		$plainIndent = 0
	): void {
		$this->ensureBodyListOpened();

		if ($plainText === "" || $plainText === true) {
			$plainText = $text;
			$text = htmlspecialchars($text);
			$text = str_replace("\n", "<br/>", $text); // convert newlines to HTML breaks
		}
		if ($plainMetaInfo === "" || $plainMetaInfo === true) {
			$plainMetaInfo = $metaInfo;
			$metaInfo = htmlspecialchars($metaInfo);
		}

		$htmlText = $text;
		if ($metaInfo) {
			$htmlText =
				'<span style="color:#6CA91C;font-weight:700;">' .
				$metaInfo .
				" </span>  " .
				$htmlText;
		}
		if ($icon !== "") {
			$icon =
				'<span style="display:block;width:16px;padding-bottom:25px;"><img style="padding-top: 3px;" src="' .
				htmlspecialchars($icon) .
				'" alt="&bull;"></span>';
		} else {
			$icon = "&bull;";
		}

		$this->htmlBody .= vsprintf($this->listItem, [$icon, $htmlText]);
		if ($plainText !== false) {
			if ($plainIndent === 0) {
				/*
		 * If plainIndent is not set by caller, this is the old NC17 layout code.
		 */
				$this->plainBody .= "  * " . $plainText;
				if ($plainMetaInfo !== false) {
					$this->plainBody .= " (" . $plainMetaInfo . ")";
				}
				$this->plainBody .= PHP_EOL;
			} else {
				/*
		 * Caller can set plainIndent > 0 to format plainText in tabular fashion.
		 * with plainMetaInfo in column 1, and plainText in column 2.
		 * The plainMetaInfo label is right justified in a field of width
		 * "plainIndent". Multilines after the first are indented plainIndent+1
		 * (to account for space after label).  Fixes: #12391
		 */
				/** @var string $label */
				$label = $plainMetaInfo !== false ? $plainMetaInfo : "";
				$this->plainBody .= sprintf(
					"%{$plainIndent}s %s\n",
					$label,
					str_replace(
						"\n",
						"\n" . str_repeat(" ", $plainIndent + 1),
						$plainText
					)
				);
			}
		}
	}
	/**
	 * Adds a paragraph to the body of the email
	 *
	 * @param string $text Note: When $plainText falls back to this, HTML is automatically escaped in the HTML email
	 * @param string|bool $plainText Text that is used in the plain text email
	 *   if empty the $text is used, if false none will be used
	 */
	public function addBodyText(string $text, $plainText = ''): void {
		if ($this->footerAdded) {
			return;
		}

		if ($plainText === "") {
			$plainText = $text;
			$text = htmlspecialchars($text);
		}
		$text = str_replace("…", "", $text);
		$text = str_replace("...", "", $text);
		$text = str_replace(
			"<a href=",
			'<a style="text-decoration: none;color: #007BFF;font-size: 18px;" href=',
			$text
		);

		$this->ensureBodyListClosed();
		$this->ensureBodyIsOpened();

		$this->htmlBody .= vsprintf($this->bodyText, [$text]);
		if ($plainText !== false) {
			$this->plainBody .= $plainText . PHP_EOL . PHP_EOL;
		}
	}
	/**
	 * Adds a button group of three  buttons to the body of the email
	 *
	 * @param string $textLeft Text of left button; Note: When $plainTextLeft falls back to this, HTML is automatically escaped in the HTML email
	 * @param string $urlLeft URL of left button
	 * @param string $textRight Text of right button; Note: When $plainTextRight falls back to this, HTML is automatically escaped in the HTML email
	 * @param string $urlRight URL of right button
	 * @param string $textRight Text of center button; Note: When $plainTextRight falls back to this, HTML is automatically escaped in the HTML email
	 * @param string $urlCenter URL of center button
	 * @param string $plainTextLeft Text of left button that is used in the plain text version - if unset the $textLeft is used
	 * @param string $plainTextRight Text of center button that is used in the plain text version - if unset the $textCenter is used
	 * @param string $plainTextRight Text of right button that is used in the plain text version - if unset the $textRight is used
	 */
	public function addBodyButtonGroupTentative(
		string $textLeft,
		string $urlLeft,
		string $textRight,
		string $urlRight,
		string $textCenter,
		string $urlCenter,
		string $plainTextLeft = "",
		string $plainTextCenter = "",
		string $plainTextRight = ""
	): void {
		if ($this->footerAdded) {
			return;
		}
		if ($plainTextLeft === "") {
			$plainTextLeft = $textLeft;
			$textLeft = htmlspecialchars($textLeft);
		}

		if ($plainTextCenter === "") {
			$plainTextCenter = $textCenter;
			$textCenter = htmlspecialchars($textCenter);
		}

		if ($plainTextRight === "") {
			$plainTextRight = $textRight;
			$textRight = htmlspecialchars($textRight);
		}

		$this->ensureBodyIsOpened();
		$this->ensureBodyListClosed();

		$color = '#5996F2';
		$textColor = '#ffffff';

		$this->htmlBody .= vsprintf($this->buttonGrouptentative, [
			$color,
			$color,
			$urlLeft,
			$color,
			$textColor,
			$textColor,
			$textLeft,
			$urlRight,
			$textRight,
			$urlCenter,
			$textCenter,
		]);
		$this->plainBody .= PHP_EOL . $plainTextLeft . ": " . $urlLeft . PHP_EOL;
		$this->plainBody .= $plainTextCenter . ": " . $urlCenter . PHP_EOL;
		$this->plainBody .= $plainTextRight . ": " . $urlRight . PHP_EOL . PHP_EOL;
	}
}
